﻿namespace CodeStage.Maintainer
{
	internal interface IShowableRecord
	{
		void Show();
	}
}
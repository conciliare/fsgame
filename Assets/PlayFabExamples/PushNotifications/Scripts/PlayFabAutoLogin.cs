﻿#if UNITY_EDITOR

#elif UNITY_ANDROID
#define PLAYFAB_ANDROID
#elif UNITY_IOS
#define PLAYFAB_IOS
#endif

using UnityEngine;

using PlayFab;
using PlayFab.ClientModels;

/// <summary>
/// Controls the automatic login used on mobile devices (iOS & Android)
/// </summary>
public class PlayFabAutoLogin : MonoBehaviour {
	public string TitleId;
	public string callStatus = "Useful for debugging your title's push notificaiton system\n\n Run on an Android or iOS device to see automatic device ID authentication.";
	public bool isAuthenticated = false;

	void Awake () {
		PlayFabSettings.TitleId = TitleId;
	}

	void Start () {
		Debug.Log("Starting Auto-login Process");
		#if PLAYFAB_IOS
		PlayFabClientAPI.LoginWithIOSDeviceID (new LoginWithIOSDeviceIDRequest
		{
			DeviceId = SystemInfo.deviceUniqueIdentifier,
			OS = SystemInfo.operatingSystem,
			DeviceModel = SystemInfo.deviceModel,
			CreateAccount = true
		}, onLoginSuccess, null);
		#elif PLAYFAB_ANDROID
		PlayFabClientAPI.LoginWithAndroidDeviceID (new LoginWithAndroidDeviceIDRequest
		{
			AndroidDeviceId = SystemInfo.deviceUniqueIdentifier,
			OS = SystemInfo.operatingSystem,
			AndroidDevice = SystemInfo.deviceModel,
			CreateAccount = true
		}, onLoginSuccess, null);
		#endif
	}
	
	/// <summary>
	/// Called after a successful PlayFab login 
	/// </summary>
	/// <param name="result">login details</param>
	private void onLoginSuccess(LoginResult result)
	{
		
		this.callStatus = string.Format("PlayFab Authentication Successful! -- Player ID:{0}", result.PlayFabId);
		this.isAuthenticated = true;
		Debug.Log("On Device: " + SystemInfo.deviceUniqueIdentifier);
		Debug.Log(callStatus);
		
	}
	
	/// <summary>
	/// Called after a login error
	/// </summary>
	/// <param name="error">Error details</param>
	private void onLoginError(PlayFabError error)
	{
		this.callStatus = string.Format("Error {0}: {1}", error.HttpCode, error.ErrorMessage);
		Debug.Log(callStatus);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class PushPromptController : MonoBehaviour {
	public Text PromptTitle;
	public Text PromptBody;
	public Button CloseButton;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void ClosePrompt()
	{
		this.gameObject.SetActive(false);
	}
}

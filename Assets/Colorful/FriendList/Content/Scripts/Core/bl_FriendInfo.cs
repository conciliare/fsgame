﻿using UnityEngine;
using UnityEngine.UI;

public class bl_FriendInfo : MonoBehaviour {

    public Text NameText = null;
    public Text StatusText = null;
    public Image StatusImage = null;
    public GameObject JoinButton = null;
    [Space(5)]
    public Color OnlineColor = new Color(0, 0.9f, 0, 0.9f);
    public Color OffLineColor = new Color(0.9f, 0, 0, 0.9f);

    private string roomName = string.Empty;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="info"></param>
    public void GetInfo(FriendInfo info)
    {
        NameText.text = info.Name;
        if (StatusText != null) { StatusText.text = (info.IsOnline) ? "[Online]" : "[OffLine]"; }
        StatusImage.color = (info.IsOnline) ? OnlineColor : OffLineColor;
        JoinButton.SetActive((info.IsInRoom) ? true : false);
        roomName = info.Room;
    }
    /// <summary>
    /// 
    /// </summary>
    public void JoinRoom()
    {
        if (!string.IsNullOrEmpty(roomName))
        {
            PhotonNetwork.JoinRoom(roomName);
        }
        else
        {
            Debug.Log("This room doesn exits more");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Remove()
    {
        bl_FriendList manager = GameObject.Find(bl_FriendList.FriendListName).GetComponent<bl_FriendList>();
        manager.RemoveFriend(NameText.text);
    }
}